#!/usr/bin/end python

# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 15:05:24 2013

This tutorial shows how to interoperate VTK 5.8+ with Qt4

Important note: this tutorial will NOT work with VTK 6.x

Qt4 is a popular cross-platform Graphical User Interface (GUI) framework written
entirely in C++.

The Python wrapper libraries are called PyQt4 and are available for free.

Qt4 is used to build complex GUIs, giving a professional look and feel to your
VTK application

Qt4 is the standard de-facto for cross-platform development and it comes in two
licensed versions: LGPL and commercial.

The only difference between the two licenses is that with the commercial license
it is legally OK to do static linking, while with LGPL only dynamic linking
is legally allowed.

If you plan to compile your Python code and sell it, it is important to be aware
of such restriction.

This program creates a Single Document Interface (SDI) application comprising a
Qt4 window, with a menu bar and a VTK central widget.

The program takes advantage of the QVTKRenderWindowInteractor class, which
produces a QWidget ready to be integrated.

The VTK widget draws a polygonal sphere with which it is possible to interact
(zoom, translation and rotation).

From the menu bar it is possible to invoke a color picker dialog to change
both the background and sphere colors independently.

@author: Alfredo Metere
@email: alfredo.metere@mmk.su.se
@address: Department of Materials and Environmental Chemistry (MMK)
          Arrhenius V. 16C
          106 91 Stockholm
          Sweden
          
"""

print "Loading Qt4 modules ...",
try:
    from PyQt4.QtCore import *
    from PyQt4.QtGui  import *
    print "Done!"
except ValueError:
    print "Could not load Qt4 modules. Check your Qt4 installation"
    raise
    
print "Loading VTK module  ...",
try:
    from vtk import *
    from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
    print "Done!"
except ValueError:
    print "Could not load VTK. Check your VTK installation"
    raise
    
print "Loading sys module  ...",
try:
    import sys
    print "Done!"
except ValueError:
    print "Cannot import sys module. Check your Python installation"
    raise



class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.init_ui()

    def progExit(self):
        quit()

    def BgColorPicker(self):
        # Yes, it is cool. The color picker widget is a default one.
        # You don't have to create your own. Pretty elegant and fast solution.
        self.colPickerDlg = QColorDialog(self)
        self.color = self.colPickerDlg.getColor()
        rgb = [self.color.redF(), self.color.greenF(), self.color.blueF()]
        
        # Since we are in the same memory space, it is possible to do this
        # These two lines update the VTK renderer with new background color
        # and provoke an update of the render window. The second line is
        # optional, but it helps Python with the automatic garbage collection
        # to keep your memory tidy. Important for complex apps.
        self.rrw.ren.SetBackground(rgb)
        self.rrw.Render()
        
    def ObjColorPicker(self):
        self.colPickerDlg = QColorDialog(self)
        self.color = self.colPickerDlg.getColor()
        rgb = [self.color.redF(), self.color.greenF(), self.color.blueF()]
        
        # Here the self.rrw.Render() is voluntarily omitted to demonstrate
        # that it is not needed. Just saving you couple of key strokes.
        self.rrw.sphereActor.GetProperty().SetColor(rgb)
    
    def showAboutDlg(self):
        # Yes, also for message dialogs some nice classes are immediately
        # available, like this one.
        self.abtDlg = QMessageBox(self)
        abtTxt = """
            Qt4 and VTK interoperability tutorial.
            Copyright Alfredo Metere, 2013.
            This code is licensed as LGPL.
            If you use it, report this copyright message.
        """
        self.abtDlg.setText(abtTxt)
        self.abtDlg.show()

    def init_ui(self):
        self.setWindowTitle("Qt4 and VTK")
        
        # Creates a RenWin QWidget from our custom Qt4 VTK class and maps it
        # into the same memory space of the main window object
        self.rrw = RenWin(self)
        
        # These two lines define the menuBar object in the Qt window
        self.menubar = self.menuBar()
        self.menubar.setVisible(True)

        # These three lines add the menu bar items
        self.fileMenu = self.menubar.addMenu("&File")
        self.optionsMenu = self.menubar.addMenu("&Options")
        self.helpMenu = self.menubar.addMenu("&Help")
        
        
        # This lines block defines the menu actions for each of the menu bar
        # items that have been just defined
        # -------------- BEGIN ------------------
        self.fileMenuExit = QAction("E&xit", self)
        
        # The following line describes a signal connection
        # In Qt, all events can be managed with a signal/slot system
        # This helps a lot to keep the code lean and readable
        # even when it becomes very complex.
        # Signals are methods to output data from a Qt class
        # Slots are methods to input data to a Qt class
        # The available slots and signals of a Qt class of course depends on
        # the inherited slots and signals and the user-defined ones.
        #
        # The general syntax is:
        #            [senderObj].[signal].connect([receiverObj].[slot])
        
        self.fileMenuExit.triggered.connect(self.progExit)

        self.optionsMenuBgColor = QAction("Change &Background Color", self)
        self.optionsMenuBgColor.triggered.connect(self.BgColorPicker)
        
        self.optionsMenuObjColor = QAction("Change &Object Color", self)
        self.optionsMenuObjColor.triggered.connect(self.ObjColorPicker)
        
        self.helpMenuAbout = QAction("&About ...", self)
        self.helpMenuAbout.triggered.connect(self.showAboutDlg)
        # ---------------- END -----------------------        
        
        # The next four lines are needed to visualize the menu actions that 
        # have just been defined
        self.fileMenu.addAction(self.fileMenuExit)
        self.optionsMenu.addAction(self.optionsMenuBgColor)
        self.optionsMenu.addAction(self.optionsMenuObjColor)
        self.helpMenu.addAction(self.helpMenuAbout)

        
        # This tells Qt that the VTK widget should fill the main window
        self.setCentralWidget(self.rrw)

        
        
class RenWin(QVTKRenderWindowInteractor):
    def __init__(self, parent=None):
        super(RenWin,self).__init__(parent)
        
        #This line is FUNDAMENTAL. There's a bug somewhere in VTK for which
        #if you do not put this re-initialization line, it won't work.
        QVTKRenderWindowInteractor().__init__()

        self.init_ui()
        
       
    def init_ui(self, parent=None):

        # Nothing really fancy down there. Quite standard VTK stuff
        self.ren = vtkRenderer()
        
        self.GetRenderWindow().AddRenderer(self.ren)
        
        self.sphereSource = vtkSphereSource()
        self.sphereSource.SetRadius(3.0)	
        
        self.sphereMapper = vtkPolyDataMapper()
        self.sphereMapper.SetInputConnection(self.sphereSource.GetOutputPort())
        
        self.sphereActor = vtkActor()
        self.sphereActor.SetMapper(self.sphereMapper)
        
        self.ren.AddActor(self.sphereActor)
        self.ren.SetBackground(0.0, 0.0, 0.0)
        
        # Changes default VTK interaction style to something more familiar.
        # You can actually customize it completely.
        self.SetInteractorStyle(vtkInteractorStyleTrackballCamera())
        self.Initialize()
        self.Start()
	

	

print "\nStarting the Qt main loop ...",
app = QApplication(sys.argv)
mw = MainWindow()
print "Done!"

mw.show()

sys.exit(app.exec_())
